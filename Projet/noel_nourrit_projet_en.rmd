---
output:
  pdf_document: 
    toc: true
  html_document:
    df_print: paged
  classoption: t
title: "STATISTICAL MODEL PROJECT"
author: "Jules NOEL - Gabriel NOURRIT"
date: "5 avril 2018"
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```





##INTRODUCTION

The data we have chosen have been retrieved from the government website: https://www.data.gouv.fr/fr/.
These data represent the insertion of a student into professional life after the Master.
They are composed of several columns, such as the university, the field of study, the discipline of study, the insertion rate, the gross average salary, the woman's rate, etc ...
We decided to focus on the insertion of a student into professional life after the Master.
Having already made a choice, this study will be an opportunity for us to support it or not.


For that we decided to address ourselves to 5 questions in particular.


* What is the integration rate of Master's graduates according to their fields of study, 30 months after graduation?
* What is the university, by field, whose average exit salary is the highest?
* 18 months after graduation, do graduates with a job have a lower median salary than their peers inserted 12 months later?
* Is the field of informatics an area with a good insertion rate and a high exit salary?
* Do areas with the highest female share have a proportional insertion rate?


Why did you choose these questions?
To identify the most promising areas in terms of hiring.
When we want to choose a field of study, with equal affinities it may be interesting to do a study:


- salary. Will domain A allow me to earn more than domain B?
- insertion. Even if domain A earns me more, will hiring be faster?
- University. Which university will be the best for me?


FINAL OBJECTIVE
And finally in its entirety our study will constitute a qualitative file that will allow us to respond to our intuitions and confirm (or not) whether our choice of studies (place and field) is appropriate and in line with the job market.

FOR FURTHER
We had exploitable data and a gender question
We decided to answer this question from an openness perspective.
If certain fields are more of women or men, does the hiring confirm this trend or do they try in an egalitarian way to recruit the opposite gender in a major way?

\clearpage

```{r echo=FALSE, error=FALSE, warning=FALSE, message=FALSE}
library(dplyr);
library(ggplot2);
library(magrittr);
library(readr);
library("gridExtra");
library("cowplot");
```


##I / Reorganization of data

  - Some data that had to be numeric were in the form of factor -> So we transformed them
  - Then we have replaced all the values not indicated by 0 to be able to treat them more easily later



```{r warning=FALSE}
#Chargement de la base
df <- read.csv(file="donnees2.csv", sep =";",dec=",");

#Changement de la colonne taux d'insertion en numérique
df$taux_dinsertion=as.numeric(gsub(",",".",as.character(df$taux_dinsertion)))
#Changement de la colonne salaire brut en numérique
df$salaire_brut_annuel_estime=
  as.numeric(levels(df$salaire_brut_annuel_estime))[df$salaire_brut_annuel_estime]
#Changement de la colonne part des femmes en numériques
df$femmes=as.numeric(levels(df$femmes))[df$femmes]

#Elimination des valeurs non renseignées
df$taux_dinsertion[is.na(df$taux_dinsertion)]=0
df$salaire_brut_annuel_estime[is.na(df$salaire_brut_annuel_estime)]=0
df$femmes[is.na(df$femmes)]=0
```



## II / Integration rate of graduates according to their fields of study 30 months after their diplomas


Which field of study has the highest insertion rate 30 months after graduation?

```{r fig.width=8,fig.height=5, echo=FALSE, warning=FALSE}
degradeNoir <- colorRampPalette(c("#000000", "#CCCCCC"))

#df%>%group_by(discipline)%>%distinct(discipline)
df_1=df%>%filter(situation=="30 mois après le diplôme")%>%filter(taux_dinsertion>0)%>%filter(discipline!="")%>%group_by(discipline)%>%summarize(mean=mean(taux_dinsertion))
#df_1[order(df_1$mean),] 
#Taux d'insertion moyen par discipline
df_1$discipline=reorder(df_1$discipline,df_1$mean)
g=df_1%>%ggplot( aes(x=reorder(discipline,mean), y=mean, fill=discipline)) + geom_bar(stat="identity", width=0.8) + coord_flip()+theme_minimal()+theme(legend.position="none") + scale_fill_manual(values=rev(degradeNoir(21)))+ggtitle("Taux d'insertion des diplômés de Master\n en fonction de leurs domaines d'étude\n 30 mois après l'obtention de leurs diplômes") +ylab("Taux d'insertion (%)") + xlab("Domaine d'étude")
g

```

We can see that there is a big gap between the different domains. The domain with the lowest insertion rate is "History-Geography" with about 78% and the domain with the highest insertion rate is "Master of Education: Second Degree, CPE ..." with about 92%. There is therefore a difference of more than 10%.
Finally to follow the thread of our record, we find that the field of computing is placed in 4 th position (almost execo with the 3 rd) which is more than honorable. Our choice therefore starts rather well, with an insertion rate only beaten by the teaching masters. Let's see what happens to the other questions.



## III / Average salary per university according to several domains

What is the university, by field, whose average exit salary is the highest?

```{r,fig.width=20,fig.height=20, echo=FALSE, warning=FALSE}
#liste de tous les domaines
#df%>%group_by(domaine)%>%distinct(domaine)

degradeBleu <- colorRampPalette(c("#003366", "#00FFFF"))

#Salaire moyen par université dans le domaine Sciences, technologies et santé
t1=df%>%filter(salaire_brut_annuel_estime>0)%>%filter(domaine=="Sciences, technologies et santé")%>%filter(etablissement!="")%>%group_by(etablissement)%>%summarize(mean=mean(salaire_brut_annuel_estime))

t1$etablissement=reorder(t1$etablissement,t1$mean)

g1=t1%>%ggplot( aes(x=etablissement, y=mean, fill=etablissement)) + coord_flip() + geom_bar(stat="identity",width = 0.8) + scale_fill_manual(values=rev(degradeBleu(61)))+theme_minimal()+theme(legend.position="none",axis.text.y = element_text(size=5),axis.text.x = element_text(size=5)) + ggtitle("Salaire moyen par université \ndans le domaine Sciences, technologies et santé") +
  ylab("Salaire moyen (euros)") + xlab("Université") + ylim(0, 40000)


#Salaire moyen par université dans le domaine Sciences humaines et sociales
t2=df%>%filter(salaire_brut_annuel_estime>0)%>%filter(domaine=="Sciences humaines et sociales")%>%filter(etablissement!="")%>%group_by(etablissement)%>%summarize(mean=mean(salaire_brut_annuel_estime))

t2$etablissement=reorder(t2$etablissement,t2$mean)

g2=t2%>%ggplot( aes(x=etablissement, y=mean, fill=etablissement)) + 
            geom_bar(stat="identity",width = 0.8) + 
            coord_flip() +
            scale_fill_manual(values=rev(degradeBleu(61))) +
            theme_minimal()+
            theme(legend.position="none",axis.text.y = element_text(size=6),axis.text.x = element_text(size=5)) +
            ggtitle("Salaire moyen par université \ndans le domaine Sciences humaines et sociales") +
            ylab("Salaire moyen (euros)") + 
            xlab("Université") + ylim(0, 40000)


t3=df%>%filter(salaire_brut_annuel_estime>0)%>%filter(domaine=="Masters enseignement")%>%filter(etablissement!="")%>%group_by(etablissement)%>%summarize(mean=mean(salaire_brut_annuel_estime))

t3$etablissement=reorder(t3$etablissement,t3$mean)

g3=t3%>%ggplot( aes(x=etablissement, y=mean, fill=etablissement)) + 
            geom_bar(stat="identity",width = 0.8) + 
            coord_flip() + 
            scale_fill_manual(values=rev(degradeBleu(61))) +
            theme_minimal()+theme(legend.position="none",axis.text.y = element_text(size=6),axis.text.x = element_text(size=5)) +
            ggtitle("Salaire moyen par université \ndans le domaine Masters enseignement") +
            ylab("Salaire moyen (euros)") + 
            xlab("Université") + ylim(0, 40000)


t4=df%>%filter(salaire_brut_annuel_estime>0)%>%filter(domaine=="Droit, économie et gestion")%>%filter(etablissement!="")%>%group_by(etablissement)%>%summarize(mean=mean(salaire_brut_annuel_estime))

t4$etablissement=reorder(t4$etablissement,t4$mean)

g4=t4%>%ggplot( aes(x=etablissement, y=mean, fill=etablissement)) + 
            geom_bar(stat="identity",width = 0.8) + 
            coord_flip() + 
            scale_fill_manual(values=rev(degradeBleu(65))) +
            theme_minimal()+theme(legend.position="none",axis.text.y = element_text(size=6),axis.text.x = element_text(size=5)) +
            ggtitle("Salaire moyen par université \ndans le domaine Droit, économie et gestion") +
            ylab("Salaire moyen (euros)") + 
            xlab("Université") + ylim(0, 40000)


t5=df%>%filter(salaire_brut_annuel_estime>0)%>%filter(domaine=="Lettres, langues, arts")%>%filter(etablissement!="")%>%group_by(etablissement)%>%summarize(mean=mean(salaire_brut_annuel_estime))

t5$etablissement=reorder(t5$etablissement,t5$mean)

g5=t5%>%ggplot( aes(x=etablissement, y=mean, fill=etablissement)) + 
            geom_bar(stat="identity",width = 0.8) + 
            coord_flip() + 
            scale_fill_manual(values=rev(degradeBleu(65)))+
            theme_minimal()+theme(legend.position="none",axis.text.y = element_text(size=6),axis.text.x = element_text(size=5)) +
            ggtitle("Salaire moyen par université \ndans le domaine Lettres, langues, arts") +
            ylab("Salaire moyen (euros)") + 
            xlab("Université") + ylim(0, 40000)

plot_grid(g1, g2, g3, g4, g5, align = "v", nrow = 2, ncol=3)
```
These data offer us many interesting readings.

a) Generally:
Here we can see that in all fields Parisian universities are always ranked first. This can be explained by the fact that the average salary in the Paris region is higher than that of the province. On the one hand to reduce the phenomenon of "Brain Drain" increased by the aura of the capital abroad, but also to meet rents and a more expensive life.

b) Placement of our faculty in Science, Technology and Health:
In the field "Science, technology and health", we observe that if we ignore the Paris basin, Joseph Fourier (~ 30K) is only about 3K Nice Nice the leader of the province. We also see that the ranking is tight until Montpellier: about 21 places (starting from Joseph fourier and ignoring the Paris basin) are played at 2K euros. This makes this University a good choice (+ 6K last) and (-3K the leader of the province). A better choice in the field (may not be in Miage) would have been Montpellier, Nice, Mulhouse or the Paris Basin with the Sorbonne far ahead. Paris is a choice to be taken with caution as mentioned before, the price of rents and life being more expensive.

c) Comparison of salaries of "Master of Science, Technology and Health" compared to other fields:


- We see that our domain forms a rather homogeneous mass around 30K euros.
- For law, economy and management we find about the same figure but with a much more ethereal mass, in fact there is a difference of about 37-23 = ~ 14K between the first and the last (38-26 = ~ 12K for science) and a 3K floor lower than science.
- About 27K average for humanities and social sciences
- About 25K for Art letters and languages
Thanks to these findings, we can say that our field is the best placed in terms of salary on the job market.

## IV / Difference insertion between 18 and 30 months

18 months after graduation, do the graduates who are inserted have a lower median salary than their classmates inserted 12 months later?

```{r, echo=FALSE, warning=FALSE}
degradeRouge <- colorRampPalette(c("#663300", "#FF9933"))

data_18=df%>%filter(situation=="18 mois après le diplôme")%>%summarize(mean=mean(salaire_brut_annuel_estime))
data_30=df%>%filter(situation=="30 mois après le diplôme")%>%summarize(mean=mean(salaire_brut_annuel_estime))

tmp <- data.frame(temps=c("18 mois après le diplome","30 mois après le diplome"),salaire_moyen_brut=c(data_18[1,],data_30[1,]))

#Salaire moyen 18 mois et 30 mois après le diplome
tmp%>%ggplot(aes(x=temps, y=salaire_moyen_brut,fill=temps))+ geom_bar(stat="identity", width = 0.5) + scale_fill_manual(values=rev(degradeRouge(2)))+theme_minimal()+theme(legend.position="none") + ggtitle("Salaire moyen brut des diplômés 18 et 30 mois\n après l'obtention de leurs diplômes") + ylab("Salaire moyen brut (euros)") + xlab("Temps")

```
We wondered whether a quick hiring was not likely to lead to a lower-paying contract. It is clear that though. So we will be more likely not to rush out of our Master.


## V / The field of computing

Is the field of informatics an area with a good insertion rate and a high exit salary?


```{r fig.width=14,fig.height=10, echo=FALSE, warning=FALSE}
#Taux d'insertion moyen par année
t1=df%>%filter(taux_dinsertion>0)%>%group_by(annee)%>%summarize(mean=mean(taux_dinsertion))%>%ggplot( aes(x=annee, y=mean)) + geom_bar(stat="identity",width = 0.7) + ggtitle("Taux d'insertion moyen par année") +
  ylab("Taux d'insertion (%)") + xlab("Année") + ylim(0, 100)

#Taux d'insertion moyen par année dans l'informatique
t2=df%>%filter(taux_dinsertion>0)%>%filter(discipline=="Informatique")%>%group_by(annee)%>%summarize(mean=mean(taux_dinsertion))%>%ggplot( aes(x=annee, y=mean)) + geom_bar(stat="identity",width = 0.7) + ggtitle("Taux d'insertion moyen par année en Informatique") +
  ylab("Taux d'insertion (%)") + xlab("Année") + ylim(0, 100)

#Salaire moyen par année
t3=df%>%filter(salaire_brut_annuel_estime>0)%>%group_by(annee)%>%summarize(mean=mean(salaire_brut_annuel_estime))%>%ggplot( aes(x=annee, y=mean)) + geom_bar(stat="identity",width = 0.7) + ggtitle("Salaire moyen brut par année") +
  ylab("Salaire moyen (euros)") + xlab("Année") +ylim(0, 40000)

#Salaire moyen par année dans l'informatique
t4=df%>%filter(salaire_brut_annuel_estime>0)%>%filter(discipline=="Informatique")%>%group_by(annee)%>%summarize(mean=mean(salaire_brut_annuel_estime))%>%ggplot( aes(x=annee, y=mean)) + geom_bar(stat="identity",width = 0.7) + ggtitle("Salaire moyen brut par année\n dans le domaine de l'informatique") +
  ylab("Salaire moyen (euros)") + xlab("Année")  + ylim(0, 40000) 

plot_grid(t1, t2, t3, t4, align = "v", nrow = 2, ncol=2)
```

Thanks to these graphs we can see that it is at the level of the rate of insertion or the average salary of exit, the domain of the computing remains above average.
For the insertion rate, the field of computer science is about 5% above average, and for the average exit salary it is above about 3,000 euros.

These graphs make a logical conclusion to all our analyzes, IT is currently a promising area and if you are passionate, you can not go wrong choosing this area.

\ clearpage

## VI / Women's share

Do areas with the highest female share have a proportional insertion rate?

```{r echo=FALSE, warning=FALSE}
data_insertion=df%>%filter(domaine!="")%>%group_by(domaine)%>%summarize(moyenne=mean(taux_dinsertion))
data_femmes=df%>%filter(domaine!="")%>%group_by(domaine)%>%filter(femmes>0)%>%summarize(moyenne=mean(femmes))
colonne_femmes=c("Part de femme","Part de femme","Part de femme","Part de femme","Part de femme")
colonne_taux=c("Taux d'insertion","Taux d'insertion","Taux d'insertion","Taux d'insertion","Taux d'insertion")
df_insertion=cbind(type=colonne_taux,data_insertion)
df_femmes=cbind(type=colonne_femmes,data_femmes)
df2=rbind(df_insertion,df_femmes)
ggplot(data=df2, aes(x=domaine, y=moyenne, fill=type)) +
  geom_bar(stat="identity", position=position_dodge())+
  coord_flip()+
  scale_fill_brewer(palette="Green")+
  theme_minimal() + ylim(0, 100) + ggtitle("Taux d'insertion et part des femmes\nen fonction des domaines d'études") +
  ylab("(%)") + xlab("Domaine d'étude")
```

We can see here that there is no correlation between women's share and the insertion rate. Indeed, some fields, such as "Letters, languages, arts" have a very high female share and a low insertion rate (almost the worst), while teaching masters have a virtually identical female rate and the best rate of education. 'insertion.


##CONCLUSION

To conclude we can say that thanks to these data we were able to determine the insertion rates and the average exit salary. Indeed, we have seen that the most important factor is the domain and discipline of study. There are disciplines such as computers that remain after several years above the average. The university is also important even if this factor remains less important than the previous one. Moreover, this study, our first in statistics, has been an interesting experience as well as a formative one. It will have allowed us to put into practice concepts learned in class such as the selection of information and the use of R.